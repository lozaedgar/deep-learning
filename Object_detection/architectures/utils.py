import torch
from torchvision import ops
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.patches as patches


def image_bboxes(image, bboxes, scale_factor=None, color='r'):

    # delete batch dim if there is
    if len(image.shape) == 4:
        image = image[0]

    if len(bboxes.shape) == 3:
        bboxes = bboxes[0]

    if scale_factor:
        height_factor, width_factor = scale_factor
        bboxes[:, [0, 2]] *= width_factor
        bboxes[:, [1, 3]] *= height_factor

    # Create figure and axes
    fig, ax = plt.subplots()

    # Display the image
    image = image.numpy().swapaxes(0, 2)
    image = image.swapaxes(0, 1)
    ax.imshow(image)

    # Create a Rectangle patch
    for i in range(bboxes.shape[0]):
        rect = patches.Rectangle((bboxes[i, 0], bboxes[i, 1]), bboxes[i, 2] - bboxes[i, 0],
                                 bboxes[i, 3] - bboxes[i, 1],
                                 linewidth=1, edgecolor=color, facecolor='none')
        # Add the patch to the Axes
        ax.add_patch(rect)

    plt.show()


def anchor_center_points(output_size):

    output_h, output_w = output_size

    center_x = torch.arange(0, output_w) + 0.5
    center_y = torch.arange(0, output_h) + 0.5

    return center_x, center_y


def anchor_boxes(output_size, ratios=[0.5, 1, 1.5], scales=[2, 4, 6]):

    center_x_anchor, center_y_anchor = anchor_center_points(output_size)

    boxes_anchor_points = torch.zeros((len(center_x_anchor) * len(center_y_anchor) * len(ratios) * len(scales), 4))
    box_idx = 0

    for cx in center_x_anchor:
        for cy in center_y_anchor:
            boxes_anchor_point = torch.zeros(4)

            for r in ratios:
                for s in scales:

                    height = s * np.sqrt(r)
                    width = s * np.sqrt(1 / r)
    
                    # save coordinates of boxes (x_min, y_min, x_max, y_max)
                    boxes_anchor_point[0] = cx - width / 2
                    boxes_anchor_point[1] = cy - height / 2
                    boxes_anchor_point[2] = cx + width / 2
                    boxes_anchor_point[3] = cy + height / 2

                    boxes_anchor_point = ops.clip_boxes_to_image(boxes_anchor_point, size=output_size)
                    boxes_anchor_points[box_idx, :] = boxes_anchor_point

                    box_idx += 1

    return torch.round(boxes_anchor_points)


def iou_matrix(gt_boxes_images, anchor_boxes):

    total_boxes = anchor_boxes.shape[0]
    batch_size = gt_boxes_images.shape[0]
    total_gt_boxes = gt_boxes_images.shape[1]

    iou = torch.zeros(batch_size, total_boxes, total_gt_boxes)

    for i in range(batch_size):

        gt_boxes_single_image = gt_boxes_images[i]
        IOU = ops.box_iou(anchor_boxes, gt_boxes_single_image)

        iou[i, :] = IOU

    return iou


def projected_gt_bboxes(gt_bboxes, scale_factors):

    projected_bboxes = gt_bboxes.clone()
    invalid_bbox_mask = (projected_bboxes == -1)  # indicating padded bboxes

    if isinstance(scale_factors, list or tuple):
        width_factor, height_factor = scale_factors
    else:
        height_factor = scale_factors
        width_factor = scale_factors

    gt_bboxes[:, :, [0, 2]] = gt_bboxes[:, :, [0, 2]] * width_factor
    gt_bboxes[:, :, [1, 3]] = gt_bboxes[:, :, [1, 3]] * height_factor

    gt_bboxes.masked_fill_(invalid_bbox_mask, -1)  # fill padded bboxes back with -1

    return torch.round(gt_bboxes)


def calculate_offsets(positive_anchors, gt_bboxes):

    positive_anchors = ops.box_convert(positive_anchors, in_fmt='xyxy', out_fmt='cxcywh')
    gt_bboxes = ops.box_convert(gt_bboxes, in_fmt='xyxy', out_fmt='cxcywh')

    gt_cx, gt_cy, gt_w, gt_h = [gt_bboxes[:, 0], gt_bboxes[:, 1], gt_bboxes[:, 2], gt_bboxes[:, 3]]
    anchors_cx, anchors_cy, anchors_w, anchors_h = [positive_anchors[:, 0], positive_anchors[:, 1],
                                                    positive_anchors[:, 2], positive_anchors[:, 3]]

    tx = (gt_cx - anchors_cx) / anchors_w
    ty = (gt_cy - anchors_cy) / anchors_h
    tw = torch.log(gt_w / anchors_w)
    th = torch.log(gt_h / anchors_h)

    return torch.stack([tx, ty, tw, th], dim=-1)


def get_proposals(anchor_boxes, gt_bboxes, gt_classes, pos_th=0.7, neg_th=0.2):

    batch_size = gt_bboxes.shape[0]
    max_boxes_batch = gt_bboxes.shape[1]
    total_anchor_boxes = anchor_boxes.shape[0]

    anchor_boxes = torch.stack([anchor_boxes for _ in range(batch_size)])

    # get the iou matrix which contains iou of every anchor box
    # against all the ground truth bboxes in an image
    iou = iou_matrix(gt_bboxes, anchor_boxes[0])

    # get max iou per gt bounding box
    max_iou_per_gt_box, _ = iou.max(dim=1, keepdim=True)

    # condition 1: the anchor box with the max iou for every gt bbox
    positive_anchor_mask = torch.logical_and(iou == max_iou_per_gt_box, max_iou_per_gt_box > 0)
    # condition 2: anchor boxes with iou above a threshold with any of the gt bboxes
    positive_anchor_mask = torch.logical_or(positive_anchor_mask, iou > pos_th)

    positive_anc_ind_sep = torch.where(positive_anchor_mask)[0]  # get separate indices in the batch
    # combine all the batches and get the idxs of the +ve anchor boxes
    positive_anchor_mask = positive_anchor_mask.flatten(start_dim=0, end_dim=1)
    positive_anchor_idxs = torch.where(positive_anchor_mask)[0]

    # get the maximum iou and the corresponding idx of gt_bbox that overlaps the most
    # with every anchor box
    max_iou_per_anchor_box, max_iou_per_anchor_box_idxs = iou.max(dim=-1)
    max_iou_per_anchor_box = max_iou_per_anchor_box.flatten(start_dim=0, end_dim=1)

    # get iou of the positive anchor boxes
    gt_iou_scores = max_iou_per_anchor_box[positive_anchor_idxs]

    # get coordinates of all ground truth boxes
    gt_bboxes_expanded = gt_bboxes.view(batch_size, 1, max_boxes_batch, 4)\
        .expand(batch_size, total_anchor_boxes, max_boxes_batch, 4)
    # for every anchor box, get the coordinates of the ground truth bbox that overlaps with the most
    max_iou_per_anchor_box_idxs_coords = max_iou_per_anchor_box_idxs.reshape(batch_size, total_anchor_boxes, 1, 1).repeat(1, 1, 1, 4)

    # make sure they're in the same device
    max_iou_per_anchor_box_idxs_coords = max_iou_per_anchor_box_idxs_coords.to(gt_bboxes_expanded.device)
    max_iou_per_anchor_box_idxs = max_iou_per_anchor_box_idxs.to(gt_bboxes_expanded.device)

    gt_bboxes = torch.gather(gt_bboxes_expanded, dim=-2, index=max_iou_per_anchor_box_idxs_coords)
    gt_bboxes_flat = gt_bboxes.flatten(start_dim=0, end_dim=2)
    gt_bboxes_positive = gt_bboxes_flat[positive_anchor_idxs]

    # GET CLASSES FOR POSITIVE ANCHOR BOXES
    # expand gt classes to map against every anchor box
    gt_classes_expand = gt_classes.view(batch_size, 1, max_boxes_batch).expand(batch_size, total_anchor_boxes, max_boxes_batch)
    # get classes for every anchor box from the gt box that overlaps the most with
    gt_classes_per_anchor = torch.gather(gt_classes_expand, dim=-1, index=max_iou_per_anchor_box_idxs.unsqueeze(-1)).squeeze(-1)
    # combine all the batches and get the mapped classes of the +ve anchor boxes
    gt_classes_per_anchor = gt_classes_per_anchor.flatten(start_dim=0, end_dim=1)
    gt_classes_pos = gt_classes_per_anchor[positive_anchor_idxs]

    # get positive anchor boxes coordinates
    anchor_boxes_flat = anchor_boxes.flatten(start_dim=0, end_dim=-2)
    positive_anchor_coords = anchor_boxes_flat[positive_anchor_idxs]

    # calculate offsets
    gt_offsets = calculate_offsets(positive_anchor_coords, gt_bboxes_positive)

    # GET NEGATIVE ANCHOR BBOXES
    negative_anchor_mask = max_iou_per_anchor_box < neg_th
    negative_anchors_idxs = torch.where(negative_anchor_mask)[0]

    # sample the same number of negative and positive anchors
    negative_anchors_idxs = negative_anchors_idxs[torch.randint(0, len(negative_anchors_idxs),
                                                                size=(len(positive_anchor_idxs),))]

    negative_anchors_coords = anchor_boxes_flat[negative_anchors_idxs]

    return positive_anchor_idxs, negative_anchors_idxs, gt_iou_scores, gt_offsets, gt_classes_pos, \
        positive_anchor_coords, negative_anchors_coords, positive_anc_ind_sep


def generate_proposals(anchors, offsets):
    # change format of the anchor boxes from 'xyxy' to 'cxcywh'
    anchors = ops.box_convert(anchors, in_fmt='xyxy', out_fmt='cxcywh')

    # apply offsets to anchors to create proposals
    proposals_ = torch.zeros_like(anchors)
    proposals_[:, 0] = anchors[:, 0] + offsets[:, 0] * anchors[:, 2]
    proposals_[:, 1] = anchors[:, 1] + offsets[:, 1] * anchors[:, 3]
    proposals_[:, 2] = anchors[:, 2] * torch.exp(offsets[:, 2])
    proposals_[:, 3] = anchors[:, 3] * torch.exp(offsets[:, 3])

    # change format of proposals back from 'cxcywh' to 'xyxy'
    proposals = ops.box_convert(proposals_, in_fmt='cxcywh', out_fmt='xyxy')

    return proposals