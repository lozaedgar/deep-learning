import torchvision
import torch
import torch.nn as nn
from torchvision import ops
import torch.nn.functional as F

from .utils import anchor_boxes, projected_gt_bboxes, get_proposals, image_bboxes, generate_proposals


class ProposalNetwork(nn.Module):

    def __init__(self, in_features, hidden_dim=512, boxes_per_anchor=9, p_dropout=0.3):
        super(ProposalNetwork, self).__init__()

        self.n_boxes = boxes_per_anchor
        self.conv = nn.Conv2d(in_features, hidden_dim, kernel_size=3, padding=1)
        self.activation = nn.ReLU()
        self.dout = nn.Dropout(p_dropout)
        self.classification_head = nn.Conv2d(hidden_dim, boxes_per_anchor, kernel_size=1)
        self.regression_head = nn.Conv2d(hidden_dim, boxes_per_anchor * 4, kernel_size=1)

    def forward(self, feature_map, positive_anchors_idxs=None, negative_anchors_idxs=None, positive_anchors_coords=None):

        if positive_anchors_idxs is None and negative_anchors_idxs is None and positive_anchors_coords is None:
            mode = 'eval'
        else:
            mode = 'train'

        output = self.conv(feature_map)
        output = self.activation(self.dout(output))

        # B x Boxes per anchor x H feature map x W feature map
        predicted_classification = self.classification_head(output)
        # B x Boxes per anchor * 4 x H feature map x W feature map
        predicted_offsets = self.regression_head(output)

        if mode == 'train':
            # get predictions for positive and negative anchors
            positive_classification = predicted_classification.flatten()[positive_anchors_idxs]
            negative_classification = predicted_classification.flatten()[negative_anchors_idxs]

            # get offsets only for positive anchors
            positive_offsets = predicted_offsets.flatten()
            positive_offsets = positive_offsets.contiguous().view(-1, 4)[positive_anchors_idxs]

            proposals = generate_proposals(positive_anchors_coords, positive_offsets)

            return positive_classification, negative_classification, positive_offsets, proposals

        elif mode == 'eval':
            return predicted_classification, predicted_offsets


class RegionProposalNetwork(nn.Module):

    def __init__(self, input_dimensions=[400, 400]):
        super(RegionProposalNetwork, self).__init__()

        # feature extractor
        backbone = torchvision.models.vgg16(weights='IMAGENET1K_V1')
        self.backbone = nn.Sequential(*list(backbone.features)[:30])  # up to layer 30 is of our interest

        # get feature map dimensions from backbone if unknown
        dummy_image = torch.zeros(1, 3, input_dimensions[0], input_dimensions[1])
        dummy_image = self.backbone(dummy_image)
        dimensions_output = torch.tensor(dummy_image.shape[-2:])  # H X W
        out_channels = dummy_image.shape[-3]
        scale_factors = [dimensions_output[0] / input_dimensions[0], dimensions_output[1] / input_dimensions[1]]

        self.output_channels = out_channels
        self.scale = scale_factors
        self.backbone_output_dim = (int(dimensions_output[0]), int(dimensions_output[1]))

        # create anchor boxes
        self.anchor_scales = [2, 4, 6]
        self.anchor_ratios = [0.5, 1, 1.5]
        self.number_anchors = len(self.anchor_scales) * len(self.anchor_ratios)
        self.anchor_boxes = anchor_boxes(self.backbone_output_dim, self.anchor_ratios, self.anchor_scales)

        # IoU thresholds for proposal method
        self.positive_th = 0.7
        self.negative_th = 0.3

        # proposal network
        self.proposal_method = ProposalNetwork(out_channels, boxes_per_anchor=self.number_anchors)

    def forward(self, images, gt_bbox, category):

        batch_size = images.shape[0]
        # extract features
        feature_map = self.backbone(images)
        # resize gt boxes to feature space
        bbox = projected_gt_bboxes(gt_bbox, self.scale)

        # get anchor boxes close to  anchor boxes
        # send to same device during training
        self.anchor_boxes = self.anchor_boxes.to(bbox.device)

        positive_anchor_idxs, negative_anchor_idxs, gt_confidence_scores, \
            gt_offsets, positive_gt_classes, positive_anchors_coords, negative_anchors_coords,\
            positive_batch_idxs = get_proposals(self.anchor_boxes, bbox, category)

        # visualize the positive and negative bounding boxes
        # image_bboxes(images, positive_anchors_coords, scale_factor=(1/self.scale[0], 1/self.scale[1]), color='b')
        # image_bboxes(images, negative_anchors_coords, scale_factor=(1 / self.scale[0], 1 / self.scale[1]), color='g')

        positive_score_predictions, negative_scores_predictions, \
            positive_offsets_predictions, proposals_predictions = self.proposal_method(feature_map, positive_anchor_idxs,
                                                                                       negative_anchor_idxs, positive_anchors_coords)

        total_loss = classification_loss(positive_score_predictions, negative_scores_predictions, batch_size) + \
                     5 * regression_loss(gt_offsets, positive_offsets_predictions, batch_size)

        return total_loss, feature_map, proposals_predictions, positive_batch_idxs, positive_gt_classes

    def inference(self, images, confidence_th=0.8, nms_th=0.5):

        batch_size = images.shape[0]
        # extract features
        feature_map = self.backbone(images)

        # confidence scores and offsets
        confidence_score_prediction, offsets_prediction = self.proposal_method(feature_map)
        confidence_score_prediction = confidence_score_prediction.view(batch_size, -1)
        offsets_prediction = offsets_prediction.view(batch_size, -1, 4)

        proposals_final = []
        confidence_final = []

        # filter proposals based on confidence and NMS thresholds
        for i in range(batch_size):

            confidence_score = torch.sigmoid(confidence_score_prediction[i])
            offsets = offsets_prediction[i]
            proposals = generate_proposals(self.anchor_boxes, offsets)

            # filter by confidence threshold
            idxs_confidence = torch.where(confidence_score >= confidence_th)[0]
            proposals = proposals[idxs_confidence]
            confidence_score = confidence_score[idxs_confidence]

            # filter by NMS threshold
            nms_idxs = ops.boxes.nms(proposals, confidence_score, nms_th)
            proposals = proposals[nms_idxs]
            confidence_score = confidence_score[nms_idxs]

            # save prediction
            proposals_final.append(proposals)
            confidence_final.append(confidence_score)

        return proposals_final, confidence_final, feature_map


# ------------------ loss functions RPN ------------------ #
def classification_loss(conf_scores_pos, conf_scores_neg, batch_size):
    target_pos = torch.ones_like(conf_scores_pos)
    target_neg = torch.zeros_like(conf_scores_neg)

    target = torch.cat((target_pos, target_neg))
    inputs = torch.cat((conf_scores_pos, conf_scores_neg))

    loss = F.binary_cross_entropy_with_logits(inputs, target, reduction='sum') * 1. / batch_size

    return loss


def regression_loss(gt_offsets, reg_offsets_pos, batch_size):
    assert gt_offsets.size() == reg_offsets_pos.size()
    loss = F.smooth_l1_loss(reg_offsets_pos, gt_offsets, reduction='sum') * 1. / batch_size
    return loss


class ClassificationModule(nn.Module):

    def __init__(self, input_channels, roi_sizes, n_classes, hidden_dim=512, p_dropout=0.3):
        super(ClassificationModule, self).__init__()

        self.roi_size = roi_sizes
        self.avg_pool = nn.AvgPool2d(self.roi_size)
        self.fc = nn.Linear(input_channels, hidden_dim)
        self.dout = nn.Dropout(p_dropout)
        self.classification_head = nn.Linear(hidden_dim, n_classes)

    def forward(self, feature_map, proposals_list, gt_classes=None):

        if gt_classes is None:
            mode = 'eval'
        else:
            mode = 'train'

        # apply roi pooling on proposals followed by avg pooling
        roi_out = ops.roi_pool(feature_map, proposals_list, self.roi_size)
        roi_out = self.avg_pool(roi_out)

        # flatten the output
        roi_out = roi_out.squeeze(-1).squeeze(-1)

        # pass the output through the hidden network
        out = self.fc(roi_out)
        out = F.relu(self.dout(out))

        # get the classification scores
        classification_scores = self.classification_head(out)

        if mode == 'eval':
            return classification_scores
        else:
            # compute cross entropy loss
            total_loss = F.cross_entropy(classification_scores, gt_classes.long())

            return total_loss


class FasterRCNN(nn.Module):

    def __init__(self, roi_size, n_classes):
        super(FasterRCNN, self).__init__()

        self.rpn = RegionProposalNetwork()
        self.classification = ClassificationModule(self.rpn.output_channels, roi_size, n_classes)

    def forward(self, images, gt_bboxes, gt_classes):

        total_rnp_loss, feature_map, proposals, \
            positive_anchors_batch, positive_gt_classes = self.rpn(images, gt_bboxes, gt_classes)

        # get separate proposals for each sample
        positive_proposals_list = []
        batch_size = images.size(dim=0)

        for idx in range(batch_size):
            proposals_idxs = torch.where(positive_anchors_batch == idx)[0]
            proposals_batch = proposals[proposals_idxs].detach().clone()
            positive_proposals_list.append(proposals_batch)

        classification_loss = self.classification(feature_map, positive_proposals_list, positive_gt_classes)
        total_loss = total_rnp_loss + classification_loss

        return total_loss

    def inference(self, images, confidence_th=0.7, nms_th=0.3):

        batch_size = images.size(dim=0)
        proposals_final, conf_scores_final, feature_map = self.rpn.inference(images, confidence_th, nms_th)
        cls_scores = self.classification(feature_map, proposals_final)

        # convert scores into probability
        cls_probs = F.softmax(cls_scores, dim=-1)
        # get classes with the highest probability
        classes_all = torch.argmax(cls_probs, dim=-1)

        classes_final = []
        # slice classes to map to their corresponding image
        c = 0
        for i in range(batch_size):
            n_proposals = len(proposals_final[i]) # get the number of proposals for each image
            classes_final.append(classes_all[c: c+n_proposals])
            c += n_proposals

        return proposals_final, conf_scores_final, classes_final