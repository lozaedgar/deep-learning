import torch
import torchvision.utils
from torch.utils.tensorboard import SummaryWriter
import numpy as np

def loop(model, optimizer, train_dataloader, validation_dataloader, epochs, device):

    writer = SummaryWriter('log')
    model = model.to(device)
    dict_categories = {item['id']: item['name'] for _, item in train_dataloader.dataset.coco.cats.items()}

    for i in range(epochs):

        print(f"Epoch {i}")

        total_loss_train = 0.0
        total_loss_val = 0.0
        model.train()

        for images, bboxes, categories in train_dataloader:

            images = images.to(device)
            bboxes = bboxes.to(device)
            categories = categories.to(device)

            loss = model(images, bboxes, categories)

            # backpropagation
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            total_loss_train += loss.item()

        model.eval()

        # validation loop
        for images_val, bboxes_val, categories_val in validation_dataloader:

            with torch.no_grad():

                images_val = images_val.to(device)
                bboxes_val = bboxes_val.to(device)
                categories_val = categories_val.to(device)

                loss = model(images_val, bboxes_val, categories_val)
                total_loss_val += loss.item()

        total_loss_train /= len(train_dataloader)
        total_loss_val /= len(validation_dataloader)

        # save weights
        torch.save(model.state_dict(), f'weights/fast_rcnn_{i}.pt')

        # save loss functions
        writer.add_scalar("Loss/Train", total_loss_train, i)
        writer.add_scalar("Loss/Validation", total_loss_val, i)

        # save example images
        images_to_draw = []
        idxs = list(torch.randint(0, len(validation_dataloader.dataset), size=(1,)))
        images = [validation_dataloader.dataset[idx][0] for idx in idxs]
        images = torch.stack(images).to(device)  # turn list into batch

        model.eval()
        bboxes, confidences, classes = model.inference(images)

        for bbox, image, confidence, classs in zip(bboxes, images, confidences, classes):
            confidence = [dict_categories[tag.item()] + " :" + str(np.round(score.item(), decimals=2))
                          for score, tag in zip(confidence, classs)]
            scale_height, scale_width = model.rpn.scale
            bbox[:, [0, 2]] *= 1 / scale_width
            bbox[:, [1, 3]] *= 1 / scale_height

            # without batch dimension, so 0 as idx
            image_bbox = torchvision.utils.draw_bounding_boxes((255. * image).byte(), bbox, labels=confidence)
            images_to_draw.append(image_bbox)

        images_to_draw = torch.stack(images_to_draw)
        grid = torchvision.utils.make_grid(images_to_draw)
        grid = grid.byte()
        writer.add_image('images', grid, i)  # remove the batch dimension

    writer.close()