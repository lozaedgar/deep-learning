import torchvision
import torch
import numpy as np
import matplotlib.pyplot as plt

import torchvision.transforms.functional as F


def show(imgs):

    if not isinstance(imgs, list):
        imgs = [imgs]

    fig, axs = plt.subplots(ncols=len(imgs), squeeze=False)

    for i, img in enumerate(imgs):
        img = img.detach()
        img = F.to_pil_image(img)
        axs[0, i].imshow(np.asarray(img))
        axs[0, i].set(xticklabels=[], yticklabels=[], xticks=[], yticks=[])

    plt.show()

def visualize_proposals(images, bboxes, confidence, classes, dict_categories, scale=(1, 1)):

    images_to_draw = []
    scale_height, scale_width = scale

    for bbox, image, confidence, classs in zip(bboxes, images, confidence, classes):
        confidence = [dict_categories[tag.item()] + " :" + str(np.round(score.item(), decimals=2))
                      for score, tag in zip(confidence, classs)]

        bbox[:, [0, 2]] *= 1 / scale_width
        bbox[:, [1, 3]] *= 1 / scale_height

        # without batch dimension, so 0 as idx
        image_bbox = torchvision.utils.draw_bounding_boxes((255. * image).byte(), bbox, labels=confidence)
        images_to_draw.append(image_bbox)

    images_to_draw = torch.stack(images_to_draw)
    grid = torchvision.utils.make_grid(images_to_draw)
    grid = grid.byte()

    show(grid)
