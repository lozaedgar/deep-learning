import torch

from architectures.FasterRCNN import FasterRCNN
from dataset.dataset_class import Aquarium
from utils.visualization import visualize_proposals

# load data
batch_size = 1
test_dataset = Aquarium(mode='test')
image, _, _ = test_dataset[5]
image = image.unsqueeze(0)  # add batch dimension

# load model
number_classes = test_dataset.classes + 1
roi_size = (2, 2)
model = FasterRCNN(roi_size, number_classes)
weights = torch.load('weights/fast_rcnn_1.pt')
model.load_state_dict(weights)
model.eval()

# variables for visualization
dict_categories = {item['id']: item['name'] for _, item in test_dataset.coco.cats.items()}
scale = model.rpn.scale

proposals, confidence, classes = model.inference(image)
print(classes)
visualize_proposals(image, proposals, confidence, classes, dict_categories, scale)

