# 1. Dataset
The images used to train the FasterRCNN can be found [here](https://public.roboflow.com/object-detection/aquarium/2/download/coco).
For this project, the dataset was downloaded with COCO format. The images should be moved to `dataset/aquarium`. Make sure your
local installation contains this folder or create it.