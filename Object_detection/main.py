from architectures.FasterRCNN import FasterRCNN
from dataset.dataset_class import Aquarium
from torch.utils.data import DataLoader
import torch.optim as optim
from utils.training_loop import loop
import torch

# load data
batch_size = 1
dataset_train = Aquarium(mode='train')
dataset_val = Aquarium(mode='valid')
dataloader_train = DataLoader(dataset_train, batch_size, shuffle=True, drop_last=True)
dataloader_val = DataLoader(dataset_val, batch_size, shuffle=True, drop_last=True)

# load model
number_classes = dataset_train.classes
roi_size = (2, 2)
model = FasterRCNN(roi_size, number_classes)

# optimizer
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
epochs = 200
lr = 1e-4
optimizer = optim.Adam(model.parameters(), lr=lr)

loop(model, optimizer, dataloader_train, dataloader_val, epochs, device)