import torch
from torch.utils.data import Dataset
from pycocotools.coco import COCO
from torch.nn.utils.rnn import pad_sequence
from torchvision.transforms import Compose, ToTensor, Resize
import os
import cv2

IMAGE_SIZE = (400, 400)


class Aquarium(Dataset):

    def __init__(self, root='dataset/aquarium', mode='train', transformation=Compose([ToTensor(),
                                                                                      Resize(IMAGE_SIZE)
                                                                                      ])):

        assert mode in ['train', 'valid', 'test']

        self.path = os.path.join(root, mode)
        self.coco = COCO(os.path.join(self.path, '_annotations.coco.json'))
        self.images = [self.coco.imgs[key]['file_name'] for key in self.coco.imgs.keys()]
        self.transform = transformation
        self.gts_bounding_boxes, self.gts_categories = self.get_all_gts()
        self.classes = len(torch.unique(self.gts_categories))

    def __len__(self):
        return len(self.images)

    def get_all_gts(self):

        bounding_boxes = []
        categories = []

        for idx in range(len(self.images)):

            bounding_boxes_per_image = []
            categories_per_image = []

            annotations_image_id = self.coco.getAnnIds(imgIds=idx)
            annotations_image = self.coco.loadAnns(annotations_image_id)

            # get scale factors
            if idx == 0:
                image = cv2.imread(os.path.join(self.path, self.images[idx]))
                scale_height, scale_width = IMAGE_SIZE[0] / image.shape[0], IMAGE_SIZE[1] / image.shape[1]

            for ann in annotations_image:

                # coco annotations are of the form (xmin, ymin, width, height)
                # we save them as (xmin, ymin, xmax, ymax)
                # the bounding boxes must be resized
                bounding_boxes_per_image.append([ann['bbox'][0] * scale_width,
                                                 ann['bbox'][1] * scale_height,
                                                 (ann['bbox'][0] + ann['bbox'][2]) * scale_width,
                                                 (ann['bbox'][1] + ann['bbox'][3]) * scale_height
                                                 ])

                categories_per_image.append(ann['category_id'])

            # for some reason annotation 432 has no bounding box
            # follow 0 convention for background
            if idx == 432:
                bounding_boxes_per_image.append([100., 100., 200., 200.])
                categories_per_image.append(0)

            bounding_boxes.append(torch.round(torch.tensor(bounding_boxes_per_image)))
            categories.append(torch.tensor(categories_per_image))

        # pad to maximum number of boxes in a single image
        bounding_boxes = pad_sequence(bounding_boxes, batch_first=True, padding_value=-1)
        categories = pad_sequence(categories, batch_first=True, padding_value=-1)

        bounding_boxes = torch.round(bounding_boxes)

        return bounding_boxes, categories

    def __getitem__(self, idx):

        # load and transform image
        image = cv2.imread(os.path.join(self.path, self.images[idx]))

        if self.transform:
            image = self.transform(image)

        # obtain bounding boxes and classes
        bounding_boxes = self.gts_bounding_boxes[idx]
        categories = self.gts_categories[idx]

        return image, bounding_boxes, categories


if __name__ == "__main__":

    dataset = Aquarium(root='aquarium')
    print(dataset.coco.cats)