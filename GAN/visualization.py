from architectures.CGAN import Generator
import torch
import matplotlib.pyplot as plt

# load model
latent_dim = 100
number_classes = 10
output_spatial_generator = [28, 28]
generator = Generator(latent_dim, output_spatial_generator, number_classes)

weights = torch.load('weights/CGAN/epoch_70.pt')
generator.load_state_dict(weights)
generator.eval()

# create several fake images
for i in range(10):
    z_vector = (2 * torch.rand((1, generator.latent)) - 1)
    label = torch.tensor([i])
    fake_image = generator(z_vector, label)

    plt.imshow(fake_image.detach().cpu().numpy()[0][0])
    plt.show(block=False)
    plt.pause(0.5)
    plt.close("all")
