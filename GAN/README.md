## Dataset
The GANs are trained with the MNIST dataset that can be directly downloaded from the Pytorch library. Make sure your
local installation contains the `GAN/datasets` folder or create it..