from torchvision.datasets import MNIST
from torch.utils.data import DataLoader
from torch.optim import RMSprop
import torch
import torchvision

from architectures.CGAN import Generator, Discriminator
from utils.wasserstein_training_loop import loop

# cpu/gpu
device = "cuda" if torch.cuda.is_available() else "cpu"

# download only training dataset, change download if needed
dataset = MNIST(root='datasets/mnist', train=True, download=False, transform=torchvision.transforms.ToTensor())
dataloader = DataLoader(dataset, shuffle=True, batch_size=64, drop_last=True)

# create models
latent_dim = 100
number_classes = 10  # we have ten digits
input_spatial_generator = [28, 28]
output_spatial_generator = list(dataset[0][0].shape[-2:])

generator = Generator(latent_dim, input_spatial_generator, number_classes)
discriminator = Discriminator(output_spatial_generator, number_classes)

# optimizers
optimizer_generator = RMSprop(generator.parameters(), lr=5e-5)
optimizer_discriminator = RMSprop(discriminator.parameters(), lr=5e-5)

# others
EPOCHS = 100

loop(generator, discriminator, optimizer_generator, optimizer_discriminator, dataloader, EPOCHS, device, labels=True)
