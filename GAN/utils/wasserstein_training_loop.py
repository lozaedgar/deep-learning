import torch
import time


def loop(generator, discriminator, optimizer_generator, optimizer_discriminator,
         dataloader, epochs, device, labels=None, n_critic=5, clipping=0.01):

    """
    Train a vanilla GAN. Saves the weights of the trained model every epoch and prints
    the corresponding losses values. Prints algo the scores (discriminator outputs)
    for the last batch per epoch.

    :param generator: instance of generator neuronal network
    :param discriminator: instance of discriminator neuronal network
    :param optimizer_generator: optimizer for generator
    :param optimizer_discriminator: optimizer for discriminator
    :param loss: loss function, must be BCE or a similar variant
    :param dataloader: dataloader of alanine configurations, normalized and flattened.
    :param epochs: training epochs
    :param device: cuda or cpu
    :return:
    """

    batch_size = dataloader.batch_size

    # send architectures to GPU if available
    generator = generator.to(device)
    discriminator = discriminator.to(device)

    for epoch in range(epochs):

        start_time = time.time()
        print('Epoch: ', epoch)

        mean_loss_generator = 0.0
        mean_loss_discriminator = 0.0

        for i, (images, digits) in enumerate(dataloader):

            # send data and noise to GPU if available
            z_vector = (2 * torch.rand((batch_size, generator.latent)) - 1).to(device)
            images = images.to(device)
            digits = digits.to(device)

            # zero gradient generator and discriminator
            optimizer_generator.zero_grad()
            optimizer_discriminator.zero_grad()

            # clipping weights in discriminator
            for parameter in discriminator.parameters():
                parameter.data.clamp_(-clipping, clipping)

            # Train discriminator
            if labels:
                # generate fake data with labels
                fake_digits = torch.randint(10, (batch_size,)).to(device)
                fake_images = generator(z_vector, fake_digits)
                # predictions
                output_d_real = discriminator(images, digits)
                output_d_fake = discriminator(fake_images.detach(), fake_digits)
            else:
                # generate fake data
                fake_images = generator(z_vector)
                # predictions
                output_d_real = discriminator(images)
                output_d_fake = discriminator(fake_images.detach())

            # wasserstein loss
            discriminator_loss = - torch.mean(output_d_real) + torch.mean(output_d_fake)
            discriminator_loss.backward()
            optimizer_discriminator.step()

            # Train generator
            if i % n_critic == 0:
                optimizer_generator.zero_grad()

                if labels:
                    fake_digits = torch.randint(10, (batch_size,)).to(device)
                    fake_images = generator(z_vector, fake_digits)
                    generator_loss = - torch.mean(discriminator(fake_images, fake_digits))
                else:
                    fake_images = generator(z_vector)
                    generator_loss = - torch.mean(discriminator(fake_images))

                generator_loss.backward()
                optimizer_generator.step()

                mean_loss_generator += generator_loss.item()

            mean_loss_discriminator += discriminator_loss.item()

        print("--- %s seconds ---" % (time.time() - start_time))
        print('Discriminator loss', mean_loss_discriminator/(len(dataloader)),
              '. Generator loss', mean_loss_generator/(len(dataloader) / n_critic))

        torch.save(generator.state_dict(), f'weights/epoch_{epoch}.pt')