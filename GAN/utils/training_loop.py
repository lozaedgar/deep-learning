import torch
import time


def loop(generator, discriminator, optimizer_generator, optimizer_discriminator,
         loss, dataloader, epochs, device, labels=None):

    """
    Train a vanilla GAN. Saves the weights of the trained model every epoch and prints
    the corresponding losses values. Prints algo the scores (discriminator outputs)
    for the last batch per epoch.

    :param generator: instance of generator neuronal network
    :param discriminator: instance of discriminator neuronal network
    :param optimizer_generator: optimizer for generator
    :param optimizer_discriminator: optimizer for discriminator
    :param loss: loss function, must be BCE or a similar variant
    :param dataloader: dataloader of alanine configurations, normalized and flattened.
    :param epochs: training epochs.
    :param device: cuda or cpu.
    :param labels: pass labels as argument to generator and discriminator.
    :return:
    """

    batch_size = dataloader.batch_size

    # send architectures to GPU if available
    generator = generator.to(device)
    discriminator = discriminator.to(device)

    # create corresponding labels and send them to device
    fake_labels = torch.zeros([batch_size, 1]).to(device)
    true_labels = torch.ones([batch_size, 1]).to(device)

    for epoch in range(epochs):

        start_time = time.time()
        print('Epoch: ', epoch)

        mean_loss_generator = 0.0
        mean_loss_discriminator = 0.0

        for image, real_digits in dataloader:

            # send data and noise to GPU if available
            z_vector = (2 * torch.rand((batch_size, generator.latent)) - 1).to(device)
            image = image.to(device)
            real_digits = real_digits.to(device)

            # generate fake data
            optimizer_generator.zero_grad()

            # create digits (cGAN) to tell generator which digit image to create
            fake_digits = torch.randint(10, (batch_size,)).to(device)
            fake_image = generator(z_vector, fake_digits) if labels else generator(z_vector)

            # Train generator
            # pass fake data to discriminator and train generator
            prediction_fake_generator = discriminator(fake_image.detach(), fake_digits) if labels \
                else discriminator(fake_image.detach())
            # loss with labels 1 (True = real_data) so generator creates data that the discriminator classifies as true
            generator_loss = loss(prediction_fake_generator, true_labels)
            # backpropagate only the generator to create "true" data. Discriminator is frozen.
            generator_loss.backward()
            optimizer_generator.step()

            # train discriminator on fake data
            # add detach so the generator weights are not affected
            pred_fake_discriminator = discriminator(fake_image.detach(), fake_digits) if labels \
                else discriminator(fake_image.detach())

            # now we pass the zero labels so discriminator learns to identify fake data
            discriminator_loss_fake = loss(pred_fake_discriminator, fake_labels)

            # train discriminator on true data
            optimizer_discriminator.zero_grad()
            pred_true_discriminator = discriminator(image, real_digits) if labels else discriminator(image)
            discriminator_loss_true = loss(pred_true_discriminator, true_labels)

            # backpropagate with both true and fake data
            discriminator_loss = (discriminator_loss_true + discriminator_loss_fake) / 2
            discriminator_loss.backward()
            optimizer_discriminator.step()

            mean_loss_generator += generator_loss.item()
            mean_loss_discriminator += discriminator_loss.item()

        # take the mean discriminator scores for the last batch of real and fake data
        # printing this value gives an idea of the actual
        fake_score = torch.mean(torch.sigmoid(pred_fake_discriminator)).item()
        real_score = torch.mean(torch.sigmoid(pred_true_discriminator)).item()

        print("--- %s seconds ---" % (time.time() - start_time))
        print('Discriminator loss', mean_loss_discriminator/(len(dataloader)),
              '. Generator loss', mean_loss_generator/(len(dataloader)))
        print('Score real:', real_score, '. Score fake:', fake_score)

        torch.save(generator.state_dict(), f'weights/epoch_{epoch}.pt')


