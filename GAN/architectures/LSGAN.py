import torch.nn as nn
import numpy as np
import torch


class Generator(nn.Module):

    def __init__(self, latent_dim, spatial_dim):
        super(Generator, self).__init__()

        # General values for architectures
        channels = [128, 128, 64, 32, 1]
        strides = [2, 2, 1, 1]
        output_paddings = [1, 1, 0, 0]
        kernel = 5

        self.reshape = [channels[0]] + [dim for dim in spatial_dim]  # C X H X W
        self.latent = latent_dim
        self.dense = nn.Linear(latent_dim, np.prod(self.reshape))
        self.batches = nn.ModuleList([nn.BatchNorm2d(channel) for channel in channels[:-1]])
        self.convTrans = nn.ModuleList([nn.ConvTranspose2d(input_channel, output_channel, kernel,
                                             stride, output_padding=output_padding, padding=2)
                          for (input_channel, output_channel, stride, output_padding) in zip(channels[:-1], channels[1:],
                                                                                           strides, output_paddings)])
        self.activation = nn.ReLU()

    def forward(self, x):

        x = self.dense(x)
        x = torch.reshape(x, [x.shape[0], *self.reshape])  # reshape to B x C x H X W

        # batch normalization + convolutional transpose block
        for batch_norm, conv_trans in zip(self.batches, self.convTrans):
            x = batch_norm(x)
            x = self.activation(x)
            x = conv_trans(x)

        return torch.sigmoid(x)


class Discriminator(nn.Module):

    def __init__(self, spatial_dim):
        super(Discriminator, self).__init__()

        # General values for architectures
        channels = [1, 32, 64, 128, 256]
        strides = [2, 2, 2, 1]
        kernel = 5

        self.conv = nn.ModuleList([nn.Conv2d(input_channel, output_channel, kernel, stride, padding=2)
                     for (input_channel, output_channel, stride) in zip(channels[:-1], channels[1:], strides)])

        out_channels = channels[-1] * np.ceil(spatial_dim[0] / 2**(len(self.conv) - 1)) \
                       * np.ceil(spatial_dim[1] / 2**(len(self.conv) - 1))

        self.linear = nn.Linear(int(out_channels), 1)
        self.activation = nn.LeakyReLU(0.2)
        self.flat = nn.Flatten()

    def forward(self, x):

        for conv in self.conv:
            x = self.activation(x)
            x = conv(x)

        x = self.flat(x)
        x = self.linear(x)

        return x


if __name__ == "__main__":

    batch = 1
    latent_dim = 100
    spatial_dim = [7, 7]

    # Generator part
    fake_image = torch.rand([batch, latent_dim])
    generator = Generator(latent_dim, spatial_dim)
    fake_image = generator(fake_image)

    print(fake_image.shape)

    # Discriminator part
    spatial_dim = list(fake_image.shape[-2:])
    discriminator = Discriminator(spatial_dim)
    score = discriminator(fake_image)

    print(score.shape)