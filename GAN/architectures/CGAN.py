import torch.nn as nn
import numpy as np
import torch


class Generator(nn.Module):

    def __init__(self, latent_dim, spatial_dim, number_classes):
        super(Generator, self).__init__()

        # General values for architectures
        channels = [128, 128, 64, 32, 1]
        strides = [2, 2, 1, 1]
        output_paddings = [1, 1, 0, 0]
        kernel = 5

        self.n_classes = number_classes

        # we divide by four because two ConvTranspose have stride two
        # and upsampling happens twice. Spatial dimension is the final [H, W].
        self.reshape = [channels[0]] + [dim // 4 for dim in spatial_dim]  # C X H X W
        self.latent = latent_dim
        self.dense = nn.Linear(latent_dim + number_classes, np.prod(self.reshape))
        self.batches = nn.ModuleList([nn.BatchNorm2d(channel) for channel in channels[:-1]])
        self.convTrans = nn.ModuleList([nn.ConvTranspose2d(input_channel, output_channel, kernel,
                                        stride, output_padding=output_padding, padding=2)
                                        for (input_channel, output_channel, stride, output_padding)
                                        in zip(channels[:-1], channels[1:], strides, output_paddings)])
        self.activation = nn.ReLU()

    def forward(self, x, y):

        # concatenate noise vector and labels
        y = nn.functional.one_hot(y, num_classes=self.n_classes)
        x = torch.concat((x, y), dim=1)  # concatenate along the noise dimension

        # CNN layers
        x = self.dense(x)
        x = torch.reshape(x, [x.shape[0], *self.reshape])  # reshape to B x C x H X W

        # batch normalization + convolutional transpose block
        for batch_norm, conv_trans in zip(self.batches, self.convTrans):
            x = batch_norm(x)
            x = self.activation(x)
            x = conv_trans(x)

        return torch.sigmoid(x)


class Discriminator(nn.Module):

    def __init__(self, spatial_dim, number_classes):
        super(Discriminator, self).__init__()

        # General values for architectures
        channels = [2, 32, 64, 128, 256]
        strides = [2, 2, 2, 1]
        kernel = 5

        self.n_classes = number_classes

        # part of model that handles the labels
        self.linear_label = nn.Linear(number_classes, int(np.product(spatial_dim)))
        self.reshape = [1] + spatial_dim  # single channel for "image" obtained from label preprocessing

        self.conv = nn.ModuleList([nn.Conv2d(input_channel, output_channel, kernel, stride, padding=2)
                                   for (input_channel, output_channel, stride)
                                   in zip(channels[:-1], channels[1:], strides)])

        out_channels = channels[-1] * np.ceil(spatial_dim[0] / 2**(len(self.conv) - 1)) \
                       * np.ceil(spatial_dim[1] / 2**(len(self.conv) - 1))

        self.linear = nn.Linear(int(out_channels), 1)
        self.activation = nn.LeakyReLU(0.2)
        self.flat = nn.Flatten()

    def forward(self, x, y):

        # one hot vector representation
        y = nn.functional.one_hot(y, num_classes=self.n_classes)
        y = y.to(torch.float32)

        # turn labels into "image", reshape and concatenate
        y = self.linear_label(y)
        y = torch.reshape(y, [x.shape[0], *self.reshape])
        # concatenate along the C channel (shape: BxCxHxW)
        x = torch.concat((x, y), dim=1)

        for conv in self.conv:
            x = self.activation(x)
            x = conv(x)

        x = self.flat(x)
        x = self.linear(x)

        return x


if __name__ == "__main__":

    batch = 1
    latent_dim = 100
    spatial_dim = [28, 28]
    n_classes = 10

    # Generator part
    fake_image = torch.rand([batch, latent_dim])
    fake_label = torch.tensor([1])
    generator = Generator(latent_dim, spatial_dim, n_classes)
    fake_image = generator(fake_image, fake_label)

    print(fake_image.shape)

    # Discriminator part
    spatial_dim = list(fake_image.shape[-2:])
    discriminator = Discriminator(spatial_dim, n_classes)
    score = discriminator(fake_image, fake_label)

    print(score.shape)