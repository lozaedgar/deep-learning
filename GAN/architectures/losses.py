import torch.nn as nn
import torch


class LSGANLoss(nn.Module):

    def __init__(self):
        super(LSGANLoss, self).__init__()

    def forward(self, output, target):
        return 0.5 * torch.mean((output-target)**2)