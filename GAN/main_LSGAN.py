from torchvision.datasets import MNIST
from torch.utils.data import DataLoader
from torch.optim import RMSprop
import torch
import torchvision
from architectures.LSGAN import Generator, Discriminator
from architectures.losses import LSGANLoss
from utils.training_loop import loop

# cpu/gpu
device = "cuda" if torch.cuda.is_available() else "cpu"

# download only training dataset, change download if needed
dataset = MNIST(root='datasets/mnist', train=True, download=False, transform=torchvision.transforms.ToTensor())
dataloader = DataLoader(dataset, shuffle=True, batch_size=64, drop_last=True)

# create models
latent_dim = 100
input_spatial_generator = [7, 7]
output_spatial_generator = list(dataset[0][0].shape[-2:])

generator = Generator(latent_dim, input_spatial_generator)
discriminator = Discriminator(output_spatial_generator)

# optimizers
optimizer_generator = RMSprop(generator.parameters(), lr=1e-4, weight_decay=3e-8)
optimizer_discriminator = RMSprop(discriminator.parameters(), lr=2e-4, weight_decay=6e-8)

# others
loss = LSGANLoss()  # nn.BCELoss()
EPOCHS = 100

loop(generator, discriminator, optimizer_generator, optimizer_discriminator, loss, dataloader, EPOCHS, device)







