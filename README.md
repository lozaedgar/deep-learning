# Deep learning portfolio 

This GitLab contains my PyTorch implementation of several codes presented in the book 
[Advanced Deep Learning with TensorFlow 2 and Keras](https://github.com/PacktPublishing/Advanced-Deep-Learning-with-Keras).
The main dataset used in this portfolio MNIST. The architectures were trained on a small GPU 
GeForce MX450 of 2GB. 

## GAN
Two main GANS (LSGAN and CGAN) were trained on the MNIST dataset. The generator and discriminator
are based on CNNs. LSGAN generates random digits images between 0 and 9. CGAN has the additional
feature to generate a picture with a specified digit. 

